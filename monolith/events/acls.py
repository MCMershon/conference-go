from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state, "per_page": 1}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": city + "," + state + "," + "US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError):
        return {
            "main_temp": None,
            "weather_desc": None,
        }
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        weather_data = {
            "main_temp": content["main"]["temp"],
            "weather_desc": content["weather"][0]["description"],
        }
    except (KeyError):
        weather_data = {
            "main_temp": None,
            "weather_desc": None,
        }
    return weather_data
